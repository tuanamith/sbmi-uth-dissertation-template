% -------------------------------------------------------------------
%  @LaTeX-class-file{
%     filename        = "Dissertate.cls",
%     version         = "2.0",
%     date            = "25 March 2014",
%     codetable       = "ISO/ASCII",
%     keywords        = "LaTeX, Dissertate",
%     supported       = "Send email to suchow@post.harvard.edu.",
%     docstring       = "Class for a dissertation."
% --------------------------------------------------------------------

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{Dissertate}[2014/03/25 v2.0 Dissertate Class]
\LoadClass[12pt, oneside, letterpaper]{book}

%
% Options
%
\RequirePackage{etoolbox}
\usepackage{hologo}
\usepackage{dashrule}
% Line spacing: dsingle/ddouble
%   Whether to use single- or doublespacing.
\newtoggle{DissertateSingleSpace}
\togglefalse{DissertateSingleSpace}
\DeclareOption{dsingle}{
    \toggletrue{DissertateSingleSpace}
    \ClassWarning{Dissertate}{Single-spaced mode on.}
}
\DeclareOption{ddouble}{\togglefalse{DissertateSingleSpace}}

\ProcessOptions\relax

% Line Spacing
%   Define two line spacings: one for the body, and one that is more compressed.
\iftoggle{DissertateSingleSpace}{
    \newcommand{\dnormalspacing}{1.2}
    \newcommand{\dcompressedspacing}{1.0}
}{
    \newcommand{\dnormalspacing}{2.0}
    \newcommand{\dcompressedspacing}{1.2}
}

% Block quote with compressed spacing
\let\oldquote\quote
\let\endoldquote\endquote
\renewenvironment{quote}
    {\begin{spacing}{\dcompressedspacing}\oldquote}
    {\endoldquote\end{spacing}}

% Itemize with compressed spacing
\let\olditemize\itemize
\let\endolditemize\enditemize
\renewenvironment{itemize}
    {\begin{spacing}{\dcompressedspacing}\olditemize}
    {\endolditemize\end{spacing}}

% Enumerate with compressed spacing
\let\oldenumerate\enumerate
\let\endoldenumerate\endenumerate
\renewenvironment{enumerate}
    {\begin{spacing}{\dcompressedspacing}\oldenumerate}
    {\endoldenumerate\end{spacing}}

% Text layout.
\RequirePackage[width=5.75in, letterpaper]{geometry}
\usepackage{ragged2e}
\RaggedRight
\RequirePackage{graphicx}
\usepackage{fixltx2e}
%\parindent 12pt
\RequirePackage{lettrine}
\RequirePackage{setspace}
\RequirePackage{verbatim}

% Fonts.
\RequirePackage{color}
\RequirePackage{xcolor}
\usepackage{hyperref}
\RequirePackage{url}
\RequirePackage{amssymb}
\RequirePackage{amsmath}
\RequirePackage{mathspec}
\setmathsfont(Digits,Latin,Greek)[Numbers={Proportional}]{EB Garamond}
\setmathrm{EB Garamond}
\AtBeginEnvironment{tabular}{\addfontfeature{RawFeature=+tnum}}
\widowpenalty=300
\clubpenalty=300
\setromanfont[Numbers=OldStyle, Ligatures={Common, TeX}, Scale=1.0]{EB Garamond}
\newfontfamily{\smallcaps}[RawFeature={+c2sc,+scmp}]{EB Garamond}
\setsansfont[Scale=MatchLowercase, BoldFont={Lato Bold}]{Lato Regular}
\setmonofont[Scale=MatchLowercase]{Source Code Pro}
\RequirePackage[labelfont={bf,sf,footnotesize,singlespacing},
                textfont={sf,footnotesize,singlespacing},
                justification={justified,RaggedRight},
                singlelinecheck=false,
                margin=0pt,
                figurewithin=chapter,
                tablewithin=chapter]{caption}
\renewcommand{\thefootnote}{\fnsymbol{footnote}}
\RequirePackage{microtype}


% Headings and headers.
\RequirePackage{fancyhdr}
\RequirePackage[tiny, md, sc]{titlesec}
\setlength{\headheight}{15pt}
\pagestyle{plain}
\RequirePackage{titling}

% Front matter.
\setcounter{tocdepth}{2}
\usepackage[titles]{tocloft}
\usepackage[titletoc]{appendix}
\renewcommand{\cftsecleader}{\cftdotfill{\cftdotsep}}
\renewcommand{\cftchapfont}{\normalsize \scshape}
\renewcommand\listfigurename{Listing of Figures}
\renewcommand\listtablename{Listing of Tables}



\renewcommand{\cftfigpresnum}{Figure }
\renewcommand{\cftfignumwidth}{4.5em}
\renewcommand{\cfttabpresnum}{Table }
\renewcommand{\cfttabnumwidth}{4.5em}


%set numbering for sections and below
\setcounter{secnumdepth}{3}

% Endmatter
\renewcommand{\setthesection}{\arabic{chapter}.A\arabic{section}}

% References.
\renewcommand\bibname{References}
%\RequirePackage[super,comma,numbers]{natbib}
\RequirePackage[authoryear]{natbib}
\renewcommand{\bibnumfmt}[1]{[#1]}
\RequirePackage[palatino]{quotchap}
\renewcommand*{\chapterheadstartvskip}{\vspace*{-0.5\baselineskip}}
\renewcommand*{\chapterheadendvskip}{\vspace{1.3\baselineskip}}

% An environment for paragraph-style section.
\providecommand\newthought[1]{%
   \addvspace{1.0\baselineskip plus 0.5ex minus 0.2ex}%
   \noindent\textsc{#1}%
}

% Align reference numbers so that they do not cause an indent.
\newlength\mybibindent
\setlength\mybibindent{30pt}
\renewenvironment{thebibliography}[1]
    {\chapter*{\bibname}%
     \@mkboth{\MakeUppercase\bibname}{\MakeUppercase\bibname}%
     \list{\@biblabel{\@arabic\c@enumiv}}
          {\settowidth\labelwidth{\@biblabel{999}}
           \leftmargin\labelwidth
            \advance\leftmargin\dimexpr\labelsep+\mybibindent\relax\itemindent-\mybibindent
           \@openbib@code
           \usecounter{enumiv}
           \let\p@enumiv\@empty
           \renewcommand\theenumiv{\@arabic\c@enumiv}}
     \sloppy
     \clubpenalty4000
     \@clubpenalty \clubpenalty
     \widowpenalty4000%
     \sfcode`\.\@m}
    {\def\@noitemerr
      {\@latex@warning{Empty `thebibliography' environment}}
     \endlist}

% Some definitions.
\def\advisor#1{\gdef\@advisor{#1}}
\def\coadvisorOne#1{\gdef\@coadvisorOne{#1}}
\def\coadvisorTwo#1{\gdef\@coadvisorTwo{#1}}
\def\committeeInternal#1{\gdef\@committeeInternal{#1}}
\def\committeeInternalOne#1{\gdef\@committeeInternalOne{#1}}
\def\committeeInternalTwo#1{\gdef\@committeeInternalTwo{#1}}
\def\committeeInternalThree#1{\gdef\@committeeInternalThree{#1}}
\def\committeeExternal#1{\gdef\@committeeExternal{#1}}
\def\degreeyear#1{\gdef\@degreeyear{#1}}
\def\degreemonth#1{\gdef\@degreemonth{#1}}
\def\degreeterm#1{\gdef\@degreeterm{#1}}
\def\degree#1{\gdef\@degree{#1}}
\def\department#1{\gdef\@department{#1}}
\def\field#1{\gdef\@field{#1}}
\def\university#1{\gdef\@university{#1}}
\def\universitycity#1{\gdef\@universitycity{#1}}
\def\universitystate#1{\gdef\@universitystate{#1}}
\def\programname#1{\gdef\@programname{#1}}
\def\pdOneName#1{\gdef\@pdOneName{#1}}
\def\pdOneField#1{\gdef\@pdOneField{#1}}
\def\pdOneSchool#1{\gdef\@pdOneSchool{#1}}
\def\pdOneYear#1{\gdef\@pdOneYear{#1}}
\def\pdTwoName#1{\gdef\@pdTwoName{#1}}
\def\pdTwoField#1{\gdef\@pdTwoField{#1}}
\def\pdTwoSchool#1{\gdef\@pdTwoSchool{#1}}
\def\pdTwoYear#1{\gdef\@pdTwoYear{#1}}
% School name and location
\university{University of Texas Health Science Center at Houston}
\universitycity{Houston}
\universitystate{Texas}

% School color found from university's graphic identity site:
% http://isites.harvard.edu/icb/icb.do?keyword=k75408&pageid=icb.page392732
\definecolor{SchoolColor}{RGB}{189, 79, 25} % Crimson
\definecolor{chaptergrey}{RGB}{0, 126, 163} % for chapter numbers
\definecolor{denimblue}{RGB}{68,105,125}
\hypersetup{
    colorlinks,
    citecolor=SchoolColor,
    filecolor=black,
    linkcolor=black,
    urlcolor=SchoolColor,
}

% Formatting guidelines found in:
% http://www.gsas.harvard.edu/publications/form_of_the_phd_dissertation.php
\renewcommand{\frontmatter}{
	\input{frontmatter/personalize}
	\maketitle
	\copyrightpage
	\dedicationpage
	\acknowledgments
	\abstractpage
	\vita
	\tableofcontents
	\listoftables
	\listoffigures % optional
}


% TODO: Make necessary edits for title page here 

\renewcommand{\maketitle}{
	\thispagestyle{empty}
	%\vspace*{\fill}
	\vspace{100pt}
	\begin{center}
	\Large \textcolor{SchoolColor}{\thetitle} \normalsize \\
	\vspace{50pt}
	\textsc{a\\ dissertation\\ 
	\vspace{15pt}
	presented to the Faculty of\\
	\@university\\
	\@department\\
	in Partial Fulfillment of the Requirements for the Degree of\\
	\vspace{15pt}
	Doctor of Philosophy\\
	\vspace*{\fill}
	By \\
	\vspace*{\fill} 
	\@author , \@pdOneName , \@pdTwoName \\
	\vspace{20pt}
	\@university\\
	\vspace*{\fill}
	\@degreeyear\\
	}
	\end{center}
	\vspace*{\fill} 
	\begin{flushleft}
		\emph{Dissertation Committee:}\\
		\vspace{15pt}
		\@advisor\textsuperscript{1}, Advisor \\
		\@committeeInternalOne\textsuperscript{1} \\
		\@committeeInternalTwo\textsuperscript{1} \\
		\@committeeInternalThree\textsuperscript{1}\\
		\@committeeExternal\textsuperscript{2},\textsuperscript{3}\\
		\vspace{15pt}
		\textsuperscript{1}\@department,\\\@university\\
		\textsuperscript{2}Harvard University\\
		\textsuperscript{3}University of Pennsylvania\\
	\end{flushleft}
%	\vspace*{\fill}
	\begin{center}
	created in \\
	\hologo{XeLaTeX}
	\end{center}
	\vspace*{\fill}
}

%TODO: Make any necessary adjustments for the signing page 

\newcommand{\signpage}{
	\newpage
	\thispagestyle{empty}
	\vspace{100pt}
	\begin{center}
		\Large \textcolor{SchoolColor}{\thetitle}\\
		\vspace{3em}\large 
		By \\
		\vspace{1.8em}
		\@author, \@pdOneName, \@pdTwoName \\
	\end{center}
	\vspace{4.5em}
	\normalsize \hspace{3.5in}APPROVED:\\
	\vspace{2em}
	\begin{flushright}
		\begin{tabular}{p{2in}p{2in}}
			& \hrulefill \\
			& \multicolumn{1}{r}{\@advisor , Chair} \\ \\
			& \hrulefill \\ 
			& \multicolumn{1}{r}{\@committeeInternalOne} \\ \\
			& \hrulefill \\
			& \multicolumn{1}{r}{\@committeeInternalTwo} \\ \\ 
			& \hrulefill \\
			& \multicolumn{1}{r}{\@committeeInternalThree} \\ \\  
			& \hrulefill \\
			& \multicolumn{1}{r}{\@committeeExternal} \\ \\ \\ \\
			\multicolumn{1}{r}{Date approved:} & \hrulefill
		\end{tabular}	
	\end{flushright}
	
	\newpage
}

\newcommand{\copyrightpage}{
	\newpage
	\thispagestyle{empty}
	\vspace*{\fill}
	\scshape \noindent \small \copyright \small \@degreeyear\hspace{3pt}-- \theauthor \\
	\noindent all rights reserved.
	\vspace*{\fill}
	\newpage
	\rm
}



\newcommand{\dedicationpage}{
	\newpage
	\pagenumbering{roman}
	\setcounter{page}{1}
	\thispagestyle{fancy} \vspace*{\fill} 
	\scshape \noindent \input{frontmatter/dedication}
	\vspace*{\fill} \newpage \rm
}

\newcommand{\acknowledgments}{
	\chapter*{Acknowledgments}
	\noindent
	\input{frontmatter/acknowledgements}
	\vspace*{\fill} \newpage
	\rm
}

\newcommand{\abstractpage}{
	\newpage
	\pagestyle{fancy}
	\lhead{Dissertation advisor: \@advisor} \rhead{\@author , \@pdTwoName}
	\renewcommand{\headrulewidth}{0.0pt}
	\vspace*{35pt}
	\begin{center}
	\Large \textcolor{SchoolColor}{\@title} \normalsize \\
	\vspace*{20pt}
	\scshape Abstract \\ \rm
	\end{center}
	\begin{doublespacing}
	\input{frontmatter/abstract}
	\end{doublespacing}
	\vspace*{\fill}
	\newpage \lhead{} \rhead{}
	\cfoot{\thepage}
}

%TODO: Make edits to the vita.tex file and vita section below

\newcommand{\vita}{
	\newpage
	\chapter*{Vita}
	%\pagenumbering{roman}
	\thispagestyle{fancy}
	\begin{doublespacing}
	\begin{flushleft}
		\@pdOneYear \hdashrule{\fill}{1pt}{1pt} \@pdOneName, \@pdOneField, \@pdOneSchool\\
		\@pdTwoYear \hdashrule{\fill}{1pt}{1pt} \@pdTwoName, \@pdTwoField, \@pdTwoSchool\\
		1981--1984 \hdashrule{\fill}{1pt}{1pt} Graduate Research Assistant, Ivory Tower State University\\
		%2014--2015 \hdashrule{\fill}{1pt}{1pt} Graduate Research Assistant, University of Texas Health Science Center at Houston\\
	\end{flushleft}
	\input{frontmatter/vita}
	\end{doublespacing}
	\vspace*{\fill}
	\begin{center}
	\scshape Field of Study\\ \rm
	\@field\\
	\end{center}
	%\newpage
	%\setcounter{page}{1}
	%\pagenumbering{arabic}
	%\rm
}


\renewcommand{\backmatter}{
    \begin{appendices}
        \include{appendices/appendixA}
    \end{appendices}
}
