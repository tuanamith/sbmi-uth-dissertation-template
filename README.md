# SBMI PhD dissertation template, a custom Dissertate LaTex template

This template follows the specifciations for the disseration (margins, page order, reference format, etc.) using Dissertate LaTex template - a template that has been used for NYU, Princeton, Stanford, and UC Berkeley PhD dissertations (https://github.com/suchow/Dissertate).

 
## Instructions

1. Follow the instructions below in "Getting started". You can ignore #4 if you use an editor.
2. Make the necessary modifications in Dissertate.cls. See the file for TODOs.
3. Use your favorite Tex editor to edit the files (I use texstudio) and compile.


## Questions & Help
email muhammad.f.amith at uth.tmc.edu
------------------

# Dissertate

## Getting started
1. Install LaTeX. For Mac OS X, we recommend MacTex (http://tug.org/mactex/); for Windows, MiKTeX (http://miktex.org/); and for Ubuntu, Tex Live (`sudo apt-get install texlive-full`)
2. Install the default fonts: EB Garamond, Lato, and Source Code Pro. The files are provided in `fonts/EB Garamond`, `fonts/Lato`, and `fonts/Source Code Pro`.
3. Personalize the document by filling out your name and all the other info in `frontmatter/personalize.md`.
4. Build your dissertation with `build.command`, located in the `scripts` directory (e.g., you can `cd` into the main directory and then run `./scripts/build.command`).

## FAQ

### How do I make the text justified instead of ragged right?
Remove or comment out the line `\RaggedRight` from the .cls file.
